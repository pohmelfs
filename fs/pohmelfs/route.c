/*
 * Copyright (C) 2011+ Evgeniy Polyakov <zbr@ioremap.net>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/slab.h>
#include <linux/workqueue.h>

#include "pohmelfs.h"


static inline int pohmelfs_route_cmp_raw(const struct pohmelfs_route *rt, const struct dnet_raw_id *raw, int group_id)
{
	if (rt->group_id < group_id)
		return -1;
	if (rt->group_id > group_id)
		return 1;

	return dnet_id_cmp_str(rt->id.id, raw->id);
}

static inline int pohmelfs_route_cmp(const struct pohmelfs_route *id1, const struct pohmelfs_route *id2)
{
	return pohmelfs_route_cmp_raw(id1, &id2->id, id2->group_id);
}

static int pohmelfs_route_insert(struct pohmelfs_connection *conn, struct pohmelfs_route *rt)
{
	struct rb_node **n = &conn->route_root.rb_node, *parent = NULL;
	struct pohmelfs_route *tmp;
	int cmp, err = 0;

	spin_lock(&conn->state_lock);
	while (*n) {
		parent = *n;

		tmp = rb_entry(parent, struct pohmelfs_route, node);

		cmp = pohmelfs_route_cmp(tmp, rt);
		if (cmp < 0)
			n = &parent->rb_left;
		else if (cmp > 0)
			n = &parent->rb_right;
		else {
			err = -EEXIST;
			goto err_out_unlock;
		}
	}

	rb_link_node(&rt->node, parent, n);
	rb_insert_color(&rt->node, &conn->route_root);

err_out_unlock:
	spin_unlock(&conn->state_lock);
	return err;
	
}

static int pohmelfs_route_add(struct pohmelfs_state *st, struct dnet_raw_id *id, int group_id)
{
	struct pohmelfs_connection *conn = st->conn;
	struct pohmelfs_route *rt;
	int err;

	rt = kmem_cache_zalloc(pohmelfs_route_cache, GFP_NOIO);
	if (!rt) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	memcpy(&rt->id, id, sizeof(struct dnet_raw_id));
	rt->group_id = group_id;
	rt->st = st;

	pohmelfs_state_get(st);

	err = pohmelfs_route_insert(conn, rt);
	if (err)
		goto err_out_put;

	rt->st->routes++;
	return 0;

err_out_put:
	pohmelfs_state_put(st);
	kmem_cache_free(pohmelfs_route_cache, rt);
err_out_exit:
	return err;
}

static struct pohmelfs_state *pohmelfs_state_lookup_connection(struct pohmelfs_connection *conn, struct dnet_raw_id *id, int group_id)
{
	struct rb_node *n = conn->route_root.rb_node;
	struct pohmelfs_route *rt;
	struct pohmelfs_state *st = NULL;
	int cmp;

	spin_lock(&conn->state_lock);
	while (n) {
		rt = rb_entry(n, struct pohmelfs_route, node);

		cmp = pohmelfs_route_cmp_raw(rt, id, group_id);

		if (!st && (rt->group_id == group_id)) {
			st = rt->st;
		}

		if (cmp < 0) {
			n = n->rb_left;

			if (rt->group_id == group_id) {
				st = rt->st;
			}
		} else if (cmp > 0)
			n = n->rb_right;
		else {
			st = rt->st;
			break;
		}
	}
	if (st)
		pohmelfs_state_get(st);

	spin_unlock(&conn->state_lock);

	return st;
}

struct pohmelfs_state *pohmelfs_state_lookup(struct pohmelfs_sb *psb, struct dnet_raw_id *id, int group_id, ssize_t size)
{
	struct pohmelfs_state *st;
	struct pohmelfs_connection *c;
	int idx;

	mutex_lock(&psb->conn_lock);
	if ((size > PAGE_SIZE) || (size < 0)) {
		idx = psb->bulk_idx;
		if (++psb->bulk_idx >= psb->bulk_num)
			psb->bulk_idx = 0;
	} else {
		/* meta connections are placed after bulk */
		idx = psb->meta_idx + psb->bulk_num;
		if (++psb->meta_idx >= psb->meta_num)
			psb->meta_idx = 0;
	}

	pr_debug("%s: selected connection: %d, group: %d, size: %zd\n",
		 pohmelfs_dump_id(id->id), idx, group_id, size);

	c = &psb->conn[idx];
	st = pohmelfs_state_lookup_connection(c, id, group_id);
	mutex_unlock(&psb->conn_lock);

	return st;
}

int pohmelfs_grab_states(struct pohmelfs_sb *psb, struct pohmelfs_state ***stp)
{
	struct pohmelfs_state **states, *st;
	struct pohmelfs_connection *c;
	int err;
	int num = 0, pos = 0;

	mutex_lock(&psb->conn_lock);
	c = &psb->conn[0];

	spin_lock(&c->state_lock);
	list_for_each_entry(st, &c->state_list, state_entry) {
		++num;
	}
	spin_unlock(&c->state_lock);
	mutex_unlock(&psb->conn_lock);

	if (!num) {
		err = -ENOENT;
		goto err_out_exit;
	}

	states = kzalloc(sizeof(struct pohmelfs_state *) * num, GFP_NOIO);
	if (!states) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	mutex_lock(&psb->conn_lock);
	c = &psb->conn[0];

	spin_lock(&c->state_lock);
	list_for_each_entry(st, &c->state_list, state_entry) {
		pohmelfs_state_get(st);
		states[pos] = st;
		++pos;
	}
	spin_unlock(&c->state_lock);
	mutex_unlock(&psb->conn_lock);

	*stp = states;
	return pos;

err_out_exit:
	return err;
}

static void pohmelfs_route_remove_nolock(struct pohmelfs_connection *conn, struct pohmelfs_route *rt)
{
	rt->st->routes--;
	rb_erase(&rt->node, &conn->route_root);
	pohmelfs_state_put(rt->st);
	kmem_cache_free(pohmelfs_route_cache, rt);
}

void pohmelfs_route_remove_all(struct pohmelfs_state *st)
{
	struct pohmelfs_connection *conn = st->conn;
	struct pohmelfs_route *rt;
	struct rb_node *n;
	int again = 1;

	while (again) {
		spin_lock(&conn->state_lock);

		n = rb_first(&conn->route_root);
		if (!n) {
			spin_unlock(&conn->state_lock);
			break;
		}

		again = 0;
		while (n) {
			rt = rb_entry(n, struct pohmelfs_route, node);

			if (rt->st == st) {
				pohmelfs_route_remove_nolock(conn, rt);
				again = 1;
				break;
			}

			n = rb_next(n);
		}
		spin_unlock(&conn->state_lock);

		cond_resched();
	}
}

static int pohmelfs_route_request_complete(struct pohmelfs_trans *t, struct pohmelfs_state *recv)
{
	struct pohmelfs_sb *psb = pohmelfs_sb(t->inode->i_sb);
	struct dnet_cmd *cmd = &recv->cmd;
	struct pohmelfs_state *st;
	struct dnet_attr *attr;
	struct dnet_addr_attr *a;
	struct dnet_raw_id *ids;
	int err = 0;

	if (!t->io_offset)
		goto err_out_exit;

	attr = t->recv_data;
	dnet_convert_attr(attr);

	if (attr->size > sizeof(struct dnet_addr_attr)) {
		int i, j, num = (attr->size - sizeof(struct dnet_addr_attr)) / sizeof(struct dnet_raw_id);

		a = (struct dnet_addr_attr *)(attr + 1);
		dnet_convert_addr_attr(a);
		ids = (struct dnet_raw_id *)(a + 1);

		mutex_lock(&psb->conn_lock);
		for (j = 0; j < psb->conn_num; ++j) {
			struct pohmelfs_connection *c = &psb->conn[j];

			st = pohmelfs_state_create(c, (struct sockaddr_storage *)&a->addr.addr, a->addr.addr_len,
					0, cmd->id.group_id);
			if (IS_ERR(st)) {
				err = PTR_ERR(st);

				if (err == -EEXIST) {
					spin_lock(&c->state_lock);
					st = pohmelfs_addr_exist(c, (struct sockaddr_storage *)&a->addr.addr, a->addr.addr_len);
					if (st) {
						st->group_id = cmd->id.group_id;
						pohmelfs_state_get(st);
						err = 0;
					}
					spin_unlock(&c->state_lock);
				}

				if (err)
					goto err_out_unlock;
			} else {
				/*
				 * reference grab logic should be the same
				 * as in case when state exist - we will drop
				 * it at the end, so we would not check whether
				 * it is new state (and refcnt == 1) or
				 * existing (refcnt > 1)
				 */
				pohmelfs_state_get(st);
			}

			for (i = 0; i < num; ++i) {
				dnet_convert_raw_id(&ids[i]);
#if 0
				pohmelfs_print_addr((struct sockaddr_storage *)&a->addr.addr, "%d:%s\n",
						cmd->id.group_id, pohmelfs_dump_id(ids[i].id));
#endif

				err = pohmelfs_route_add(st, &ids[i], cmd->id.group_id);
				if (err) {
					if (err != -EEXIST) {
						/* remove this state from route table */
						spin_lock(&c->state_lock);
						list_del_init(&st->state_entry);
						spin_unlock(&c->state_lock);

						/* drop abovementioned refcnt */
						pohmelfs_state_put(st);

						pohmelfs_state_kill(st);
						goto err_out_exit;
					}

					err = 0;
				}
			}

			/* drop abovementioned refcnt */
			pohmelfs_state_put(st);
		}
err_out_unlock:
		mutex_unlock(&psb->conn_lock);
	}

err_out_exit:
	return err;
}

int pohmelfs_route_request(struct pohmelfs_state *st)
{
	struct pohmelfs_sb *psb = st->conn->psb;
	struct pohmelfs_io *pio;
	int err;

	pio = kmem_cache_zalloc(pohmelfs_io_cache, GFP_NOIO);
	if (!pio) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	pio->pi = psb->root;
	pio->id = &psb->root->id;
	pio->cmd = DNET_CMD_ROUTE_LIST;
	pio->cflags = DNET_FLAGS_DIRECT | DNET_FLAGS_NEED_ACK;
	pio->cb.complete = pohmelfs_route_request_complete;

	err = pohmelfs_send_buf_single(pio, st);
	if (err) {
		pohmelfs_print_addr(&st->sa, "%s: %d\n", __func__, err);
		goto err_out_free;
	}
	pohmelfs_print_addr(&st->sa, "route request sent\n");

err_out_free:
	kmem_cache_free(pohmelfs_io_cache, pio);
err_out_exit:
	return err;
}
