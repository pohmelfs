/*
 * Copyright (C) 2011+ Evgeniy Polyakov <zbr@ioremap.net>
 */

#include <linux/in.h>
#include <linux/in6.h>
#include <linux/net.h>

#include <net/sock.h>
#include <net/tcp.h>

#include "pohmelfs.h"

static void pohmelfs_reconnect(struct work_struct *work)
{
	struct pohmelfs_connection *conn = container_of(to_delayed_work(work), struct pohmelfs_connection, reconnect_work);
	struct pohmelfs_reconnect *r, *tmp;
	struct pohmelfs_state *st, *stmp;
	LIST_HEAD(head);
	int err;

	mutex_lock(&conn->reconnect_lock);
	list_for_each_entry_safe(r, tmp, &conn->reconnect_list, reconnect_entry) {
		st = pohmelfs_state_create(conn, &r->sa, r->addrlen, 1, r->group_id);
		if (IS_ERR(st)) {
			err = PTR_ERR(st);

			if (err != -EEXIST)
				continue;
		} else {
			pohmelfs_print_addr(&st->sa, "reconnected\n");
		}

		list_del(&r->reconnect_entry);
		kfree(r);
	}
	mutex_unlock(&conn->reconnect_lock);

	spin_lock(&conn->state_lock);
	list_for_each_entry_safe(st, stmp, &conn->kill_state_list, state_entry) {
		list_move(&st->state_entry, &head);
	}
	spin_unlock(&conn->state_lock);

	list_for_each_entry_safe(st, stmp, &head, state_entry) {
		list_del_init(&st->state_entry);
		pohmelfs_state_kill(st);
	}

	if (!list_empty(&conn->reconnect_list) && !conn->need_exit)
		queue_delayed_work(conn->wq, &conn->reconnect_work, conn->psb->reconnect_timeout);
}

void pohmelfs_pool_clean(struct pohmelfs_connection *conn, int conn_num)
{
	struct pohmelfs_connection *c;
	struct pohmelfs_state *st, *tmp;
	struct pohmelfs_reconnect *r, *rtmp;
	int i;

	if (!conn || !conn_num)
		return;

	for (i = 0; i < conn_num; ++i) {
		c = &conn[i];

		c->need_exit = 1;

		cancel_delayed_work_sync(&c->reconnect_work);

		list_for_each_entry_safe(st, tmp, &c->state_list, state_entry) {
			list_del_init(&st->state_entry);

			pohmelfs_state_kill(st);
		}

		list_for_each_entry_safe(st, tmp, &c->kill_state_list, state_entry) {
			list_del_init(&st->state_entry);
			pohmelfs_state_kill(st);
		}

		list_for_each_entry_safe(r, rtmp, &c->reconnect_list, reconnect_entry) {
			list_del(&r->reconnect_entry);
			kfree(r);
		}

		destroy_workqueue(c->wq);
	}

	kfree(conn);
}

int pohmelfs_pool_resize(struct pohmelfs_sb *psb, int num)
{
	int err = 0, old_conn_num, i;
	struct pohmelfs_connection *conn, *old_conn, *c;
	struct pohmelfs_addr *a;
	char name[16];

	conn = kzalloc(num * sizeof(struct pohmelfs_connection), GFP_NOIO);
	if (!conn) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	for (i = 0; i < num; ++i) {
		c = &conn[i];

		c->psb = psb;
		c->idx = i;

		c->route_root = RB_ROOT;
		spin_lock_init(&c->state_lock);
		INIT_LIST_HEAD(&c->state_list);

		INIT_LIST_HEAD(&c->kill_state_list);

		mutex_init(&c->reconnect_lock);
		INIT_LIST_HEAD(&c->reconnect_list);

		INIT_DELAYED_WORK(&c->reconnect_work, pohmelfs_reconnect);

		snprintf(name, sizeof(name), "pohmelfs-%d-%d", psb->bdi_num, i);
		c->wq = alloc_workqueue(name, WQ_NON_REENTRANT | WQ_UNBOUND | WQ_FREEZABLE | WQ_MEM_RECLAIM, 0);
		if (!c->wq) {
			err = -ENOMEM;
			old_conn = conn;
			old_conn_num = i;
			goto err_out_free;
		}

		mutex_lock(&psb->conn_lock);
		list_for_each_entry(a, &psb->addr_list, addr_entry) {
			pohmelfs_state_create(c, &a->sa, a->addrlen, 1, 0);
		}
		mutex_unlock(&psb->conn_lock);

	}

	mutex_lock(&psb->conn_lock);
	old_conn = psb->conn;
	old_conn_num = psb->conn_num;

	psb->conn = conn;
	psb->conn_num = num;

	psb->meta_num = psb->conn_num / 8 + 1;
	psb->bulk_num = psb->conn_num - psb->meta_num;

	psb->meta_idx = 0;
	psb->bulk_idx = 0;
	mutex_unlock(&psb->conn_lock);
	err = 0;

err_out_free:
	pohmelfs_pool_clean(old_conn, old_conn_num);
err_out_exit:
	return err;
}
