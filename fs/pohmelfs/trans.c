/*
 * Copyright (C) 2011+ Evgeniy Polyakov <zbr@ioremap.net>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/slab.h>
#include <linux/workqueue.h>

#include "pohmelfs.h"

static void pohmelfs_trans_free(struct pohmelfs_trans *t)
{
	iput(t->inode);

	kmem_cache_free(pohmelfs_trans_cache, t);
}

static void pohmelfs_trans_release(struct kref *kref)
{
	struct pohmelfs_trans *t = container_of(kref, struct pohmelfs_trans, refcnt);
	struct pohmelfs_inode *pi = pohmelfs_inode(t->inode);

	pr_debug("%s: %lu, io_offset: %llu, ino: %ld\n",
		 pohmelfs_dump_id(pi->id.id), t->trans, t->io_offset,
		 t->inode->i_ino);

	if (t->cb.destroy)
		t->cb.destroy(t);

	pohmelfs_state_put(t->st);

	kfree(t->data);
	kfree(t->recv_data);
	pohmelfs_trans_free(t);
}

void pohmelfs_trans_put(struct pohmelfs_trans *t)
{
	kref_put(&t->refcnt, pohmelfs_trans_release);
}

struct pohmelfs_trans *pohmelfs_trans_alloc(struct inode *inode)
{
	struct pohmelfs_trans *t;
	int err;

	t = kmem_cache_zalloc(pohmelfs_trans_cache, GFP_NOIO);
	if (!t) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	kref_init(&t->refcnt);

	t->inode = igrab(inode);
	if (!t->inode) {
		err = -ENOENT;
		goto err_out_free;
	}

	return t;

err_out_free:
	kmem_cache_free(pohmelfs_trans_cache, t);
err_out_exit:
	return ERR_PTR(err);
}

static int pohmelfs_buf_complete(struct pohmelfs_trans *t, struct pohmelfs_state *recv)
{
	struct pohmelfs_inode *pi = pohmelfs_inode(t->inode);
	struct dnet_cmd *cmd = &recv->cmd;
	unsigned long long trans = cmd->trans & ~DNET_TRANS_REPLY;

	pr_debug("%s: %llu, flags: %x\n",
		 pohmelfs_dump_id(pi->id.id), trans, cmd->flags);

	return 0;
}

static int pohmelfs_buf_recv(struct pohmelfs_trans *t, struct pohmelfs_state *recv)
{
	struct dnet_cmd *cmd = &recv->cmd;
	int err;

	if (!t->recv_data) {
		t->recv_data = kmalloc(cmd->size, GFP_NOIO);
		if (!t->recv_data) {
			err = -ENOMEM;
			goto err_out_exit;
		}

		t->io_offset = 0;
	}

	err = pohmelfs_data_recv(recv, t->recv_data + t->io_offset, cmd->size - t->io_offset, MSG_DONTWAIT);
	if (err < 0)
		goto err_out_exit;

	t->io_offset += err;
	err = 0;

err_out_exit:
	return err;
}

static int pohmelfs_init_callbacks(struct pohmelfs_trans *t, struct pohmelfs_io *pio)
{
	int err = 0;
	struct pohmelfs_state *st = t->st;

	t->priv = pio->priv;
	t->cb = pio->cb;

	if (!t->cb.complete)
		t->cb.complete = pohmelfs_buf_complete;

	if (!t->cb.recv_reply)
		t->cb.recv_reply = pohmelfs_buf_recv;

	if (t->cb.init) {
		err = t->cb.init(t);
		if (err)
			goto err_out_exit;
	}

	pohmelfs_trans_insert(t);

	pohmelfs_state_schedule(st);
	pohmelfs_state_put(st);

err_out_exit:
	return err;
}

int pohmelfs_send_io_group(struct pohmelfs_io *pio, int group)
{
	struct pohmelfs_inode *pi = pio->pi;
	struct inode *inode = &pi->vfs_inode;
	struct pohmelfs_sb *psb = pohmelfs_sb(inode->i_sb);
	struct pohmelfs_state *st;
	struct pohmelfs_trans *t;
	struct dnet_cmd *cmd;
	struct dnet_attr *attr;
	struct dnet_io_attr *io;
	u64 iosize = pio->size;
	u64 alloc_io_size = pio->size;
	int err;

	/* Dirty hack to prevent setting cmd/attr size to pio->size,
	 * since in read command we specify in io->size number bytes we want,
	 * and it should not be accounted in the packet we send to remote node
	 */
	if (pio->cmd == DNET_CMD_READ)
		alloc_io_size = 0;

	t = pohmelfs_trans_alloc(inode);
	if (IS_ERR(t)) {
		err = PTR_ERR(t);
		goto err_out_exit;
	}

	st = pohmelfs_state_lookup(psb, pio->id, group, pio->size);
	if (!st) {
		err = -ENOENT;
		goto err_out_free;
	}

	t->st = st;

	/*
	 * We already hold a reference grabbed in pohmelfs_state_lookup(), it is dropped when transaction is destroyed
	 * We have to have valid state pointer to schedule sending, but after transaction is inserted into state's list,
	 * it can be processed immediately and freed and grabbed reference pointer will dissapear.
	 */
	pohmelfs_state_get(st);

	cmd = &t->cmd.cmd;
	attr = &t->cmd.attr;
	io = &t->cmd.p.io;

	dnet_setup_id(&cmd->id, group, pio->id->id);
	cmd->flags = pio->cflags;
	cmd->trans = t->trans = atomic_long_inc_return(&psb->trans);
	cmd->size = alloc_io_size + sizeof(struct dnet_io_attr) + sizeof(struct dnet_attr);

	attr->cmd = pio->cmd;
	attr->size = alloc_io_size + sizeof(struct dnet_io_attr);
	attr->flags = pio->aflags;

	memcpy(io->id, pio->id->id, DNET_ID_SIZE);
	memcpy(io->parent, pio->id->id, DNET_ID_SIZE);
	io->flags = pio->ioflags;
	io->size = iosize;
	io->offset = pio->offset;
	io->type = pio->type;
	io->start = pio->start;
	io->num = pio->num;

	t->header_size = sizeof(struct dnet_cmd) + sizeof(struct dnet_attr) + sizeof(struct dnet_io_attr);
	t->data_size = alloc_io_size;

	dnet_convert_cmd(cmd);
	dnet_convert_attr(attr);
	dnet_convert_io_attr(io);

	t->wctl = pio->wctl;

	if (pio->data) {
		if (pio->alloc_flags & POHMELFS_IO_OWN) {
			t->data = pio->data;
		} else {
			t->data = kmalloc(alloc_io_size, GFP_NOIO);
			if (!t->data) {
				err = -ENOMEM;
				goto err_out_put_state;
			}

			memcpy(t->data, pio->data, alloc_io_size);
		}
	}

	err = pohmelfs_init_callbacks(t, pio);
	if (err)
		goto err_out_put_state;


	return 0;

err_out_put_state:
	pohmelfs_state_put(t->st);
err_out_free:
	pohmelfs_trans_free(t);
err_out_exit:
	return err;
}

int pohmelfs_send_io(struct pohmelfs_io *pio)
{
	struct pohmelfs_sb *psb = pohmelfs_sb(pio->pi->vfs_inode.i_sb);
	int i, err, err_num;

	err = -ENOENT;
	err_num = 0;

	for (i = 0; i < psb->group_num; ++i) {
		err = pohmelfs_send_io_group(pio, psb->groups[i]);
		if (err)
			err_num++;
	}

	return (err_num == psb->group_num) ? err : 0;
}

int pohmelfs_trans_insert(struct pohmelfs_trans *t)
{
	struct pohmelfs_state *st = t->st;

	mutex_lock(&st->trans_lock);
	list_add_tail(&t->trans_entry, &st->trans_list);
	mutex_unlock(&st->trans_lock);

	return 0;
}

void pohmelfs_trans_remove(struct pohmelfs_trans *t)
{
	struct pohmelfs_state *st = t->st;

	mutex_lock(&st->trans_lock);
	rb_erase(&t->trans_node, &st->trans_root);
	mutex_unlock(&st->trans_lock);
}

static inline long pohmelfs_trans_cmp(struct pohmelfs_trans *t1, long trans)
{
	return t1->trans - trans;
}

/* Must be called under st->trans_lock */
int pohmelfs_trans_insert_tree(struct pohmelfs_state *st, struct pohmelfs_trans *t)
{
	struct rb_node **n = &st->trans_root.rb_node, *parent = NULL;
	struct pohmelfs_trans *tmp;
	int err = 0;
	long cmp;

	while (*n) {
		parent = *n;

		tmp = rb_entry(parent, struct pohmelfs_trans, trans_node);

		cmp = pohmelfs_trans_cmp(tmp, t->trans);
		if (cmp < 0)
			n = &parent->rb_left;
		else if (cmp > 0)
			n = &parent->rb_right;
		else {
			err = -EEXIST;
			goto err_out_exit;
		}
	}

	rb_link_node(&t->trans_node, parent, n);
	rb_insert_color(&t->trans_node, &st->trans_root);

err_out_exit:
	return err;
	
}

struct pohmelfs_trans *pohmelfs_trans_lookup(struct pohmelfs_state *st, struct dnet_cmd *cmd)
{
	struct pohmelfs_trans *t, *found = NULL;
	u64 trans = cmd->trans & ~DNET_TRANS_REPLY;
	struct rb_node *n = st->trans_root.rb_node;
	long cmp;

	mutex_lock(&st->trans_lock);
	while (n) {
		t = rb_entry(n, struct pohmelfs_trans, trans_node);

		cmp = pohmelfs_trans_cmp(t, trans);
		if (cmp < 0) {
			n = n->rb_left;
		} else if (cmp > 0)
			n = n->rb_right;
		else {
			found = t;
			kref_get(&t->refcnt);
			break;
		}
	}
	mutex_unlock(&st->trans_lock);

	return found;
}

int pohmelfs_send_buf_single(struct pohmelfs_io *pio, struct pohmelfs_state *st)
{
	struct pohmelfs_inode *pi = pio->pi;
	struct inode *inode = &pi->vfs_inode;
	struct pohmelfs_sb *psb = pohmelfs_sb(inode->i_sb);
	struct pohmelfs_trans *t;
	struct dnet_cmd *cmd;
	struct dnet_attr *attr;
	int err;

	t = pohmelfs_trans_alloc(inode);
	if (IS_ERR(t)) {
		err = PTR_ERR(t);
		goto err_out_exit;
	}

	if (!st) {
		st = pohmelfs_state_lookup(psb, pio->id, pio->group_id, pio->size);
		if (!st) {
			err = -ENOENT;
			goto err_out_free;
		}
	} else {
		pohmelfs_state_get(st);
	}

	t->st = st;
	pohmelfs_state_get(st);

	cmd = &t->cmd.cmd;
	attr = &t->cmd.attr;

	dnet_setup_id(&cmd->id, st->group_id, pio->id->id);
	cmd->flags = pio->cflags;
	cmd->trans = t->trans = atomic_long_inc_return(&psb->trans);
	cmd->size = pio->size + sizeof(struct dnet_attr);

	attr->cmd = pio->cmd;
	attr->size = pio->size;
	attr->flags = pio->aflags;

	t->header_size = sizeof(struct dnet_cmd) + sizeof(struct dnet_attr);
	t->data_size = pio->size;

	dnet_convert_cmd(cmd);
	dnet_convert_attr(attr);

	if (pio->data) {
		if (pio->alloc_flags & POHMELFS_IO_OWN) {
			t->data = pio->data;
		} else {
			t->data = kmalloc(pio->size, GFP_NOIO);
			if (!t->data) {
				err = -ENOMEM;
				goto err_out_put_state;
			}

			memcpy(t->data, pio->data, pio->size);
		}
	}

	err = pohmelfs_init_callbacks(t, pio);
	if (err)
		goto err_out_put_state;

	return 0;

err_out_put_state:
	pohmelfs_state_put(t->st);
err_out_free:
	pohmelfs_trans_free(t);
err_out_exit:
	return err;
}

int pohmelfs_send_buf(struct pohmelfs_io *pio)
{
	struct pohmelfs_sb *psb = pohmelfs_sb(pio->pi->vfs_inode.i_sb);
	int i, err, err_num;

	err = -ENOENT;
	err_num = 0;

	for (i = 0; i < psb->group_num; ++i) {
		pio->group_id = psb->groups[i];

		err = pohmelfs_send_buf_single(pio, NULL);
		if (err)
			err_num++;
	}

	return (err_num == psb->group_num) ? err : 0;
}
