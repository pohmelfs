/*
 * Copyright (C) 2011+ Evgeniy Polyakov <zbr@ioremap.net>
 */

#ifndef __POHMELFS_H
#define __POHMELFS_H

#include <linux/backing-dev.h>
#include <linux/crypto.h>
#include <linux/fs.h>
#include <linux/kref.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/net.h>
#include <linux/pagemap.h>
#include <linux/pagevec.h>
#include <linux/printk.h>
#include <linux/slab.h>
#include <linux/time.h>
#include <linux/wait.h>
#include <linux/workqueue.h>

#include <crypto/sha.h>

#define dnet_bswap16(x)		cpu_to_le16(x)
#define dnet_bswap32(x)		cpu_to_le32(x)
#define dnet_bswap64(x)		cpu_to_le64(x)

/* theese are needed for packet.h below to compile */
#define DNET_ID_SIZE	SHA512_DIGEST_SIZE
#define DNET_CSUM_SIZE	SHA512_DIGEST_SIZE

#define POHMELFS_INODE_COLUMN		3

/*
 * is not used in kernel, but we want to share the same header
 * with userspace, so I put it here for compiler to shut up
 */
int gettimeofday(struct timeval *, struct timezone *);

#include "packet.h"

static inline struct timespec pohmelfs_date(struct dnet_time *tm)
{
	struct timespec ts;

	ts.tv_sec = tm->tsec;
	ts.tv_nsec = tm->tnsec;

	return ts;
}

struct pohmelfs_cmd {
	struct dnet_cmd		cmd;
	struct dnet_attr	attr;
	union {
		struct dnet_io_attr	io;
	} p;
};

/*
 * Compare two IDs.
 * Returns  1 when id1 > id2
 *         -1 when id1 < id2
 *          0 when id1 = id2
 */
static inline int dnet_id_cmp_str(const unsigned char *id1, const unsigned char *id2)
{
	unsigned int i = 0;

	for (i*=sizeof(unsigned long); i<DNET_ID_SIZE; ++i) {
		if (id1[i] < id2[i])
			return -1;
		if (id1[i] > id2[i])
			return 1;
	}

	return 0;
}

struct pohmelfs_state;
struct pohmelfs_sb;
struct pohmelfs_trans;

struct pohmelfs_trans_cb {
	int				(* init)(struct pohmelfs_trans *t);
	int				(* complete)(struct pohmelfs_trans *t, struct pohmelfs_state *recv);
	int				(* recv_reply)(struct pohmelfs_trans *t, struct pohmelfs_state *recv);
	void				(* destroy)(struct pohmelfs_trans *t);
};

struct pohmelfs_trans {
	struct list_head	trans_entry;
	struct rb_node		trans_node;

	struct kref		refcnt;

	unsigned long		trans;

	struct inode		*inode;

	struct pohmelfs_state	*st;

	struct pohmelfs_cmd	cmd;

	u64			header_size, data_size;

	unsigned long long	io_offset;

	void			*data;
	void			*recv_data;

	struct pohmelfs_write_ctl	*wctl;
	void			*priv;

	struct pohmelfs_trans_cb	cb;
};

struct pohmelfs_trans *pohmelfs_trans_alloc(struct inode *inode);
struct pohmelfs_trans *pohmelfs_trans_alloc_io_buf(struct inode *inode, int group, int command,
		void *data, u64 offset, u64 size, int aflags, int ioflags, int type);
void pohmelfs_trans_put(struct pohmelfs_trans *t);

int pohmelfs_trans_insert(struct pohmelfs_trans *t);
int pohmelfs_trans_insert_tree(struct pohmelfs_state *st, struct pohmelfs_trans *t);
void pohmelfs_trans_remove(struct pohmelfs_trans *t);
struct pohmelfs_trans *pohmelfs_trans_lookup(struct pohmelfs_state *st, struct dnet_cmd *cmd);

struct pohmelfs_state {
	struct pohmelfs_connection	*conn;
	struct list_head	state_entry;

	struct sockaddr_storage	sa;
	int			addrlen;
	struct socket		*sock;

	int			group_id;

	struct mutex		trans_lock;
	struct list_head	trans_list;
	struct rb_root		trans_root;

	struct kref		refcnt;

	int			routes;

	/* Waiting/polling machinery */
	wait_queue_t		wait;
	wait_queue_head_t	*whead;

	struct work_struct	io_work;

	/* is set when dnet_cmd is being read, otherwise attached data */
	int			cmd_read;
	/* currently read command reply */
	struct dnet_cmd		cmd;

	uint64_t		bsize;		/* Block size */
	uint64_t		frsize;		/* Fragment size */
	uint64_t		blocks;		/* Filesystem size in frsize units */
	uint64_t		bfree;		/* # free blocks */
	uint64_t		bavail;		/* # free blocks for non-root */
};

struct pohmelfs_state *pohmelfs_state_create(struct pohmelfs_connection *conn, struct sockaddr_storage *sa, int addrlen,
		int ask_route, int group_id);
struct pohmelfs_state *pohmelfs_state_lookup(struct pohmelfs_sb *psb, struct dnet_raw_id *id, int group, ssize_t size);
int pohmelfs_grab_states(struct pohmelfs_sb *psb, struct pohmelfs_state ***stp);

static inline void pohmelfs_state_get(struct pohmelfs_state *st)
{
	kref_get(&st->refcnt);
}

void pohmelfs_state_put(struct pohmelfs_state *st);
void pohmelfs_state_kill(struct pohmelfs_state *st);

struct pohmelfs_state *pohmelfs_addr_exist(struct pohmelfs_connection *conn, struct sockaddr_storage *sa, int addrlen);

void pohmelfs_state_schedule(struct pohmelfs_state *st);

__attribute__ ((format (printf, 2, 3))) void pohmelfs_print_addr(struct sockaddr_storage *addr, const char *fmt, ...);

#define POHMELFS_INODE_INFO_REMOVED		(1<<0)

struct pohmelfs_inode_info {
	struct dnet_raw_id	id;

	unsigned int		mode;
	unsigned int		nlink;
	unsigned int		uid;
	unsigned int		gid;
	unsigned int		blocksize;
	unsigned int		namelen;
	__u64			ino;
	__u64			blocks;
	__u64			rdev;
	__u64			size;
	__u64			version;

	__u64			flags;

	struct dnet_time	ctime;
	struct dnet_time	mtime;
	struct dnet_time	atime;
} __attribute__ ((packed));

void pohmelfs_fill_inode_info(struct inode *inode, struct pohmelfs_inode_info *info);
void pohmelfs_fill_inode(struct inode *inode, struct pohmelfs_inode_info *info);
void pohmelfs_convert_inode_info(struct pohmelfs_inode_info *info);

struct pohmelfs_inode {
	struct inode		vfs_inode;
	struct dnet_raw_id	id;

	struct rb_node		node;

	struct mutex		lock;

	int			*groups;
	int			group_num;

	time_t			update;
	int			local;
};

int pohmelfs_send_dentry(struct pohmelfs_inode *pi, struct dnet_raw_id *id, const char *sname, int len, int sync);
struct pohmelfs_inode *pohmelfs_sb_inode_lookup(struct pohmelfs_sb *psb, struct dnet_raw_id *id);

struct pohmelfs_reconnect {
	struct list_head	reconnect_entry;
	struct sockaddr_storage	sa;
	int			addrlen;
	int			group_id;
};

int pohmelfs_state_add_reconnect(struct pohmelfs_state *st);

struct pohmelfs_path {
	struct mutex		lock;
	char			*data;
};

int pohmelfs_http_compat_id(struct pohmelfs_inode *pi);

struct pohmelfs_addr {
	struct list_head	addr_entry;
	struct sockaddr_storage	sa;
	int			addrlen;
};

struct pohmelfs_connection {
	struct pohmelfs_sb	*psb;

	int			idx;

	struct rb_root		route_root;
	struct list_head	state_list;
	spinlock_t		state_lock;

	struct mutex		reconnect_lock;
	struct list_head	reconnect_list;
	struct list_head	kill_state_list;

	struct workqueue_struct	*wq;

	int			need_exit;
	struct delayed_work	reconnect_work;
};

void pohmelfs_pool_clean(struct pohmelfs_connection *conn, int conn_num);
int pohmelfs_pool_resize(struct pohmelfs_sb *psb, int num);

struct pohmelfs_sb {
	struct super_block	*sb;
	struct backing_dev_info	bdi;

	struct pohmelfs_inode	*root;

	spinlock_t		inode_lock;
	struct rb_root		inode_root;

	int			http_compat;
	struct pohmelfs_path	*path;

	int			bdi_num;

	struct pohmelfs_connection	*conn;
	int			conn_num;
	int			bulk_idx, bulk_num;
	int			meta_idx, meta_num;
	struct mutex		conn_lock;

	/* protected by conn_lock */
	struct list_head	addr_list;

	long			read_wait_timeout;
	long			write_wait_timeout;
	long			sync_timeout;
	long			reconnect_timeout;

	int			need_exit;
	struct delayed_work	sync_work;
	struct workqueue_struct	*wq;

	char			*fsid;
	int			fsid_len;

	atomic_long_t		ino;
	atomic_long_t		trans;

	struct crypto_hash	*hash;

	int			*groups;
	int			group_num;

	/*
	 * number of copies to be successfully written to mark write as successful
	 * if not set, half of groups plus one must be successfully written, i.e. plain write quorum
	 */
	int			successful_write_count;
	int			keepalive_cnt, keepalive_interval, keepalive_idle;
	int			readdir_allocation;
	int			sync_on_close;
	int			no_read_csum;
};

static inline struct pohmelfs_sb *pohmelfs_sb(struct super_block *sb)
{
	return (struct pohmelfs_sb *)sb->s_fs_info;
}

static inline struct pohmelfs_inode *pohmelfs_inode(struct inode *inode)
{
	return container_of(inode, struct pohmelfs_inode, vfs_inode);
}

struct pohmelfs_wait {
	wait_queue_head_t	wq;
	struct pohmelfs_inode	*pi;
	void			*ret;
	atomic_long_t		count;
	int			condition;
	struct kref		refcnt;
};

int pohmelfs_wait_init(struct pohmelfs_wait *wait, struct pohmelfs_inode *pi);
struct pohmelfs_wait *pohmelfs_wait_alloc(struct pohmelfs_inode *pi);
void pohmelfs_wait_put(struct pohmelfs_wait *wait);
static inline void pohmelfs_wait_get(struct pohmelfs_wait *wait)
{
	kref_get(&wait->refcnt);
}

struct pohmelfs_inode_info_binary_package {
	struct pohmelfs_inode_info	info;

	struct pohmelfs_wait		wait;
};

struct pohmelfs_write_ctl {
	struct pagevec			pvec;
	struct pohmelfs_inode_info	*info;

	struct kref			refcnt;
	atomic_t			good_writes;
};

struct pohmelfs_dentry_disk {
	struct dnet_raw_id		id;
	uint64_t			ino;
	int				type;
	int				len;
	char				name[0];
} __attribute__((packed));

struct pohmelfs_dentry {
	struct dnet_raw_id		parent_id;
	struct pohmelfs_dentry_disk	disk;
};

extern struct kmem_cache *pohmelfs_inode_cache;
extern struct kmem_cache *pohmelfs_trans_cache;
extern struct kmem_cache *pohmelfs_inode_info_cache;
extern struct kmem_cache *pohmelfs_route_cache;
extern struct kmem_cache *pohmelfs_wait_cache;
extern struct kmem_cache *pohmelfs_io_cache;
extern struct kmem_cache *pohmelfs_inode_info_binary_package_cache;
extern struct kmem_cache *pohmelfs_write_cache;
extern struct kmem_cache *pohmelfs_dentry_cache;

struct inode *pohmelfs_alloc_inode(struct super_block *sb);
void pohmelfs_destroy_inode(struct inode *);

struct pohmelfs_inode *pohmelfs_existing_inode(struct pohmelfs_sb *psb, struct pohmelfs_inode_info *info);
struct pohmelfs_inode *pohmelfs_new_inode(struct pohmelfs_sb *psb, int mode);
int pohmelfs_hash(struct pohmelfs_sb *psb, const void *data, const size_t size, struct dnet_raw_id *id);

char *pohmelfs_dump_id(const unsigned char *id);
char *pohmelfs_dump_id_len_raw(const unsigned char *id, unsigned int len, char *dst);

int pohmelfs_write_command(struct pohmelfs_inode *pi, struct pohmelfs_write_ctl *ctl, loff_t offset, size_t len);
void pohmelfs_write_ctl_release(struct kref *kref);
int pohmelfs_metadata_inode(struct pohmelfs_inode *pi, int sync);

extern const struct file_operations pohmelfs_dir_fops;
extern const struct inode_operations pohmelfs_dir_inode_operations;

extern const struct file_operations pohmelfs_file_ops;
extern const struct inode_operations pohmelfs_file_inode_operations;

extern const struct inode_operations pohmelfs_symlink_inode_operations;
extern const struct inode_operations pohmelfs_special_inode_operations;

extern void *pohmelfs_scratch_buf;
extern int pohmelfs_scratch_buf_size;

/*
 * if this flag is set, pohmelfs_inode_info->data is owned by the caller,
 * so sending path may use it on its own and free (using kfree) when it's done
 *
 * This logic does not work for shared buffers or
 * when multiple transactions will be sent for single pohmelfs_inode_info
 */
#define POHMELFS_IO_OWN			(1<<0)

struct pohmelfs_io {
	struct pohmelfs_inode		*pi;

	struct dnet_raw_id		*id;

	int				cmd;
	int				type;

	u64				offset, size;
	u64				start, num;

	u32				cflags;
	u32				aflags;
	u32				ioflags;

	int				group_id;

	u32				alloc_flags;
	void				*data;

	struct pohmelfs_write_ctl	*wctl;
	void				*priv;

	struct pohmelfs_trans_cb	cb;
};

int pohmelfs_send_io_group(struct pohmelfs_io *pio, int group_id);
int pohmelfs_send_io(struct pohmelfs_io *pio);
int pohmelfs_send_buf_single(struct pohmelfs_io *pio, struct pohmelfs_state *st);
int pohmelfs_send_buf(struct pohmelfs_io *pio);

int pohmelfs_data_recv(struct pohmelfs_state *st, void *buf, u64 size, unsigned int flags);
int pohmelfs_recv(struct pohmelfs_trans *t, struct pohmelfs_state *recv, void *data, int size);

struct pohmelfs_route {
	struct rb_node			node;
	int				group_id;
	struct dnet_raw_id		id;
	struct pohmelfs_state		*st;
};

int pohmelfs_route_request(struct pohmelfs_state *st);
void pohmelfs_route_remove_all(struct pohmelfs_state *st);

struct pohmelfs_script_req {
	char			*obj_name;
	int			obj_len;

	char			*script_name;
	int			script_namelen;

	void			*binary;
	int			binary_size;

	int			group_id;

	unsigned int		cflags;
	int			sync;

	struct dnet_raw_id	*id;

	int			(* complete)(struct pohmelfs_trans *t, struct pohmelfs_state *recv);
	void			*ret;
	int			ret_cond;
};

int pohmelfs_send_script_request(struct pohmelfs_inode *parent, struct pohmelfs_script_req *req);

int pohmelfs_stat(struct pohmelfs_sb *psb, int sync);

static inline int pohmelfs_need_resync(struct pohmelfs_inode *pi)
{
	struct pohmelfs_sb *psb = pohmelfs_sb(pi->vfs_inode.i_sb);
	return get_seconds() > pi->update + psb->sync_timeout;
}

#endif /* __POHMELFS_H */
