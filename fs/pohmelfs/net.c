/*
 * Copyright (C) 2011+ Evgeniy Polyakov <zbr@ioremap.net>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/in.h>
#include <linux/in6.h>
#include <linux/net.h>

#include <net/sock.h>
#include <net/tcp.h>

#include "pohmelfs.h"

void *pohmelfs_scratch_buf;
int pohmelfs_scratch_buf_size = 4096;

void pohmelfs_print_addr(struct sockaddr_storage *addr, const char *fmt, ...)
{
	struct sockaddr *sa = (struct sockaddr *)addr;
	struct va_format vaf;
	va_list args;

	va_start(args, fmt);
	vaf.fmt = fmt;
	vaf.va = &args;

	if (sa->sa_family == AF_INET) {
		struct sockaddr_in *sin = (struct sockaddr_in *)addr;
		pr_info("%pI4:%d: %pV",
			&sin->sin_addr.s_addr, ntohs(sin->sin_port), &vaf);
	} else if (sa->sa_family == AF_INET6) {
		struct sockaddr_in6 *sin = (struct sockaddr_in6 *)addr;
		pr_info("%pI6:%d: %pV",
			&sin->sin6_addr, ntohs(sin->sin6_port), &vaf);
	}

	va_end(args);
}

/*
 * Basic network sending/receiving functions.
 * Blocked mode is used.
 */
int pohmelfs_data_recv(struct pohmelfs_state *st, void *buf, u64 size, unsigned int flags)
{
	struct msghdr msg;
	struct kvec iov;
	int err;

	BUG_ON(!size);

	iov.iov_base = buf;
	iov.iov_len = size;

	msg.msg_iov = (struct iovec *)&iov;
	msg.msg_iovlen = 1;
	msg.msg_name = NULL;
	msg.msg_namelen = 0;
	msg.msg_control = NULL;
	msg.msg_controllen = 0;
	msg.msg_flags = flags;

	err = kernel_recvmsg(st->sock, &msg, &iov, 1, iov.iov_len, msg.msg_flags);
	if (err < 0)
		goto err_out_exit;

err_out_exit:
	return err;
}

int pohmelfs_recv(struct pohmelfs_trans *t, struct pohmelfs_state *recv, void *data, int size)
{
	int err;

	err = pohmelfs_data_recv(recv, data, size, MSG_DONTWAIT);
	if (err < 0)
		return err;

	t->io_offset += err;
	return err;
}

static int pohmelfs_data_send(struct pohmelfs_trans *t)
{
	struct msghdr msg;
	struct iovec io;
	int err;

	msg.msg_name = NULL;
	msg.msg_namelen = 0;
	msg.msg_control = NULL;
	msg.msg_controllen = 0;
	msg.msg_flags = MSG_DONTWAIT;

	msg.msg_iov = &io;
	msg.msg_iovlen = 1;


	if (t->io_offset < t->header_size) {
		io.iov_base = (void *)(&t->cmd) + t->io_offset;
		io.iov_len = t->header_size - t->io_offset;

		err = kernel_sendmsg(t->st->sock, &msg, (struct kvec *)msg.msg_iov, 1, io.iov_len);
		if (err < 0) {
			if (err == 0)
				err = -ECONNRESET;
			goto err_out_exit;
		}

		t->io_offset += err;
	}

	if ((t->io_offset >= t->header_size) && t->data) {
		size_t sent_size = t->io_offset - t->header_size;
		io.iov_base = t->data + sent_size;
		io.iov_len = t->data_size - sent_size;

		err = kernel_sendmsg(t->st->sock, &msg, (struct kvec *)msg.msg_iov, 1, io.iov_len);
		if (err < 0) {
			if (err == 0)
				err = -ECONNRESET;
			goto err_out_exit;
		}

		t->io_offset += err;
	}


	err = 0;

err_out_exit:
	return err;
}

static int pohmelfs_page_send(struct pohmelfs_trans *t)
{
	struct pohmelfs_write_ctl *ctl = t->wctl;
	struct msghdr msg;
	struct iovec io;
	unsigned i;
	int err = -EINVAL;

	if (t->io_offset < t->header_size) {
		io.iov_base = (void *)(&t->cmd) + t->io_offset;
		io.iov_len = t->header_size - t->io_offset;

		msg.msg_name = NULL;
		msg.msg_namelen = 0;
		msg.msg_control = NULL;
		msg.msg_controllen = 0;
		msg.msg_flags = MSG_DONTWAIT;

		msg.msg_iov = &io;
		msg.msg_iovlen = 1;

		err = kernel_sendmsg(t->st->sock, &msg, (struct kvec *)msg.msg_iov, 1, io.iov_len);
		if (err < 0) {
			if (err == 0)
				err = -ECONNRESET;
			goto err_out_exit;
		}

		t->io_offset += err;
	}

	if (t->io_offset >= t->header_size) {
		size_t skip_offset = 0;
		size_t size = le64_to_cpu(t->cmd.cmd.size) + sizeof(struct dnet_cmd) - t->io_offset;
		size_t current_io_offset = t->io_offset - t->header_size;

		for (i = 0; i < pagevec_count(&ctl->pvec); ++i) {
			struct page *page = ctl->pvec.pages[i];
			size_t sz = PAGE_CACHE_SIZE;

			if (sz > size)
				sz = size;

			if (current_io_offset > skip_offset + sz) {
				skip_offset += sz;
				continue;
			}

			sz -= current_io_offset - skip_offset;

			err = kernel_sendpage(t->st->sock, page, current_io_offset - skip_offset, sz, MSG_DONTWAIT);

			pr_debug("%s: %d/%d: total-size: %llu, io-offset: %llu, rest-size: %zd, current-io: %zd, skip-offset: %zd, sz: %zu: %d\n",
				 pohmelfs_dump_id(pohmelfs_inode(t->inode)->id.id),
				 i, pagevec_count(&ctl->pvec),
				 (unsigned long long)le64_to_cpu(t->cmd.cmd.size) + sizeof(struct dnet_cmd),
				 t->io_offset, size, current_io_offset,
				 skip_offset, sz, err);

			if (err <= 0) {
				if (err == 0)
					err = -ECONNRESET;
				goto err_out_exit;
			}

			current_io_offset += err;
			skip_offset = current_io_offset;
			size -= err;
			t->io_offset += err;

			err = 0;
		}
	}

err_out_exit:
	return err;
}

/*
 * Polling machinery.
 */

struct pohmelfs_poll_helper {
	poll_table 		pt;
	struct pohmelfs_state	*st;
};

static int pohmelfs_queue_wake(wait_queue_t *wait, unsigned mode, int sync, void *key)
{
	struct pohmelfs_state *st = container_of(wait, struct pohmelfs_state, wait);

	if (!st->conn->need_exit)
		queue_work(st->conn->wq, &st->io_work);
	return 0;
}

static void pohmelfs_queue_func(struct file *file, wait_queue_head_t *whead, poll_table *pt)
{
	struct pohmelfs_state *st = container_of(pt, struct pohmelfs_poll_helper, pt)->st;

	st->whead = whead;

	init_waitqueue_func_entry(&st->wait, pohmelfs_queue_wake);
	add_wait_queue(whead, &st->wait);
}

static void pohmelfs_poll_exit(struct pohmelfs_state *st)
{
	if (st->whead) {
		remove_wait_queue(st->whead, &st->wait);
		st->whead = NULL;
	}
}

static int pohmelfs_poll_init(struct pohmelfs_state *st)
{
	struct pohmelfs_poll_helper ph;

	ph.st = st;
	init_poll_funcptr(&ph.pt, &pohmelfs_queue_func);

	st->sock->ops->poll(NULL, st->sock, &ph.pt);
	return 0;
}

static int pohmelfs_revents(struct pohmelfs_state *st, unsigned mask)
{
	unsigned revents;

	revents = st->sock->ops->poll(NULL, st->sock, NULL);
	if (revents & mask)
		return 0;

	if (revents & (POLLERR | POLLHUP | POLLNVAL | POLLRDHUP | POLLREMOVE)) {
		pohmelfs_print_addr(&st->sa, "error revents: %x\n", revents);
		return -ECONNRESET;
	}

	return -EAGAIN;
}

static int pohmelfs_state_send(struct pohmelfs_state *st)
{
	struct pohmelfs_trans *t = NULL;
	int trans_put = 0;
	size_t size;
	int err = -EAGAIN;

	mutex_lock(&st->trans_lock);
	if (!list_empty(&st->trans_list))
		t = list_first_entry(&st->trans_list, struct pohmelfs_trans, trans_entry);
	mutex_unlock(&st->trans_lock);

	if (!t)
		goto err_out_exit;

	err = pohmelfs_revents(st, POLLOUT);
	if (err)
		goto err_out_exit;

	size = le64_to_cpu(t->cmd.cmd.size) + sizeof(struct dnet_cmd);
	pr_debug("%s: starting sending: %llu/%zd\n",
		 pohmelfs_dump_id(pohmelfs_inode(t->inode)->id.id),
		 t->io_offset, size);

	if (t->wctl)
		err = pohmelfs_page_send(t);
	else
		err = pohmelfs_data_send(t);

	pr_debug("%s: sent: %llu/%zd: %d\n",
		 pohmelfs_dump_id(pohmelfs_inode(t->inode)->id.id),
		 t->io_offset, size, err);
	if (!err && (t->io_offset == size)) {
		mutex_lock(&st->trans_lock);
			list_del_init(&t->trans_entry);
			err = pohmelfs_trans_insert_tree(st, t);
			if (err)
				trans_put = 1;
			t->io_offset = 0;
		mutex_unlock(&st->trans_lock);
	}

	BUG_ON(t->io_offset > size);

	if (trans_put)
		pohmelfs_trans_put(t);

	if ((err < 0) && (err != -EAGAIN))
		goto err_out_exit;

err_out_exit:
	return err;
}

static void pohmelfs_suck_scratch(struct pohmelfs_state *st)
{
	struct dnet_cmd *cmd = &st->cmd;
	int err = 0;

	pr_debug("%llu\n", (unsigned long long)cmd->size);

	while (cmd->size) {
		int sz = pohmelfs_scratch_buf_size;

		if (cmd->size < sz)
			sz = cmd->size;

		err = pohmelfs_data_recv(st, pohmelfs_scratch_buf, sz, MSG_WAITALL);
		if (err < 0) {
			pohmelfs_print_addr(&st->sa, "recv-scratch err: %d\n", err);
			goto err_out_exit;
		}

		cmd->size -= err;
	}

err_out_exit:
	st->cmd_read = 1;
}

static int pohmelfs_state_recv(struct pohmelfs_state *st)
{
	struct dnet_cmd *cmd = &st->cmd;
	struct pohmelfs_trans *t;
	unsigned long long trans;
	int err;

	err = pohmelfs_revents(st, POLLIN);
	if (err)
		goto err_out_exit;

	if (st->cmd_read) {
		err = pohmelfs_data_recv(st, cmd, sizeof(struct dnet_cmd), MSG_WAITALL);
		if (err <= 0) {
			if (err == 0)
				err = -ECONNRESET;

			pohmelfs_print_addr(&st->sa, "recv error: %d\n", err);
			goto err_out_exit;
		}

		dnet_convert_cmd(cmd);

		trans = cmd->trans & ~DNET_TRANS_REPLY;
		st->cmd_read = 0;
	}

	t = pohmelfs_trans_lookup(st, cmd);
	if (!t) {
		pohmelfs_suck_scratch(st);

		err = 0;
		goto err_out_exit;
	}
	if (cmd->size && (t->io_offset != cmd->size)) {
		err = t->cb.recv_reply(t, st);
		if (err && (err != -EAGAIN)) {
			pohmelfs_print_addr(&st->sa, "recv-reply error: %d\n", err);
			goto err_out_remove;
		}

		if (t->io_offset != cmd->size)
			goto err_out_put;
	}

	err = t->cb.complete(t, st);
	if (err) {
		pohmelfs_print_addr(&st->sa, "recv-complete err: %d\n", err);
	}

	kfree(t->recv_data);
	t->recv_data = NULL;
	t->io_offset = 0;

err_out_remove:
	/* only remove and free transaction if there is error or there will be no more replies */
	if (!(cmd->flags & DNET_FLAGS_MORE) || err) {
		pohmelfs_trans_remove(t);

		/*
		 * refcnt was grabbed twice:
		 *   in pohmelfs_trans_lookup()
		 *   and at transaction creation
		 */
		pohmelfs_trans_put(t);
	}
	st->cmd_read = 1;
	if (err) {
		cmd->size -= t->io_offset;
		t->io_offset = 0;
	}

err_out_put:
	pohmelfs_trans_put(t);
err_out_exit:
	return err;
}

static void pohmelfs_state_io_work(struct work_struct *work)
{
	struct pohmelfs_state *st = container_of(work, struct pohmelfs_state, io_work);
	int send_err, recv_err;

	send_err = recv_err = -EAGAIN;
	while (!st->conn->psb->need_exit) {
		send_err = pohmelfs_state_send(st);
		if (send_err && (send_err != -EAGAIN)) {
			pohmelfs_print_addr(&st->sa, "state send error: %d\n", send_err);
			goto err_out_exit;
		}

		recv_err = pohmelfs_state_recv(st);
		if (recv_err && (recv_err != -EAGAIN)) {
			pohmelfs_print_addr(&st->sa, "state recv error: %d\n", recv_err);
			goto err_out_exit;
		}

		if ((send_err == -EAGAIN) && (recv_err == -EAGAIN))
			break;
	}

err_out_exit:
	if ((send_err && (send_err != -EAGAIN)) || (recv_err && (recv_err != -EAGAIN))) {
		pohmelfs_state_add_reconnect(st);
	}
	return;
}

struct pohmelfs_state *pohmelfs_addr_exist(struct pohmelfs_connection *conn, struct sockaddr_storage *sa, int addrlen)
{
	struct pohmelfs_state *st;

	list_for_each_entry(st, &conn->state_list, state_entry) {
		if (st->addrlen != addrlen)
			continue;

		if (!memcmp(&st->sa, sa, addrlen)) {
			return st;
		}
	}

	return 0;
}

struct pohmelfs_state *pohmelfs_state_create(struct pohmelfs_connection *conn, struct sockaddr_storage *sa, int addrlen,
		int ask_route, int group_id)
{
	int err = 0;
	struct pohmelfs_state *st;
	struct sockaddr *addr = (struct sockaddr *)sa;

	/* early check - this state can be inserted into route table, no need to create state and check again */
	spin_lock(&conn->state_lock);
	if (pohmelfs_addr_exist(conn, sa, addrlen))
		err = -EEXIST;
	spin_unlock(&conn->state_lock);

	if (err)
		goto err_out_exit;

	st = kzalloc(sizeof(struct pohmelfs_state), GFP_KERNEL);
	if (!st) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	st->conn = conn;
	mutex_init(&st->trans_lock);
	INIT_LIST_HEAD(&st->trans_list);
	st->trans_root = RB_ROOT;

	st->group_id = group_id;

	kref_init(&st->refcnt);

	INIT_WORK(&st->io_work, pohmelfs_state_io_work);

	st->cmd_read = 1;

	err = sock_create_kern(addr->sa_family, SOCK_STREAM, IPPROTO_TCP, &st->sock);
	if (err) {
		pohmelfs_print_addr(sa, "sock_create: failed family: %d, err: %d\n", addr->sa_family, err);
		goto err_out_free;
	}

	st->sock->sk->sk_allocation = GFP_NOIO;
	st->sock->sk->sk_sndtimeo = st->sock->sk->sk_rcvtimeo = msecs_to_jiffies(60000);

	err = 1;
	sock_setsockopt(st->sock, SOL_SOCKET, SO_KEEPALIVE, (char *)&err, 4);

	tcp_setsockopt(st->sock->sk, SOL_TCP, TCP_KEEPIDLE, (char *)&conn->psb->keepalive_idle, 4);
	tcp_setsockopt(st->sock->sk, SOL_TCP, TCP_KEEPINTVL, (char *)&conn->psb->keepalive_interval, 4);
	tcp_setsockopt(st->sock->sk, SOL_TCP, TCP_KEEPCNT, (char *)&conn->psb->keepalive_cnt, 4);

	err = kernel_connect(st->sock, (struct sockaddr *)addr, addrlen, 0);
	if (err) {
		pohmelfs_print_addr(sa, "kernel_connect: failed family: %d, err: %d\n", addr->sa_family, err);
		goto err_out_release;
	}
	st->sock->sk->sk_sndtimeo = st->sock->sk->sk_rcvtimeo = msecs_to_jiffies(60000);

	memcpy(&st->sa, sa, sizeof(struct sockaddr_storage));
	st->addrlen = addrlen;

	err = pohmelfs_poll_init(st);
	if (err)
		goto err_out_shutdown;


	spin_lock(&conn->state_lock);
	err = -EEXIST;
	if (!pohmelfs_addr_exist(conn, sa, addrlen)) {
		list_add_tail(&st->state_entry, &conn->state_list);
		err = 0;
	}
	spin_unlock(&conn->state_lock);

	if (err)
		goto err_out_poll_exit;

	if (ask_route) {
		err = pohmelfs_route_request(st);
		if (err)
			goto err_out_poll_exit;
	}

	pohmelfs_print_addr(sa, "%d: connected\n", st->conn->idx);

	return st;

err_out_poll_exit:
	pohmelfs_poll_exit(st);
err_out_shutdown:
	st->sock->ops->shutdown(st->sock, 2);
err_out_release:
	sock_release(st->sock);
err_out_free:
	kfree(st);
err_out_exit:
	if (err != -EEXIST) {
		pohmelfs_print_addr(sa, "state creation failed: %d\n", err);
	}
	return ERR_PTR(err);
}

static void pohmelfs_state_exit(struct pohmelfs_state *st)
{
	if (!st->sock)
		return;

	pohmelfs_poll_exit(st);
	st->sock->ops->shutdown(st->sock, 2);

	pohmelfs_print_addr(&st->sa, "disconnected\n");
	sock_release(st->sock);
}

static void pohmelfs_state_release(struct kref *kref)
{
	struct pohmelfs_state *st = container_of(kref, struct pohmelfs_state, refcnt);
	pohmelfs_state_exit(st);
}

void pohmelfs_state_put(struct pohmelfs_state *st)
{
	kref_put(&st->refcnt, pohmelfs_state_release);
}

static void pohmelfs_state_clean(struct pohmelfs_state *st)
{
	struct pohmelfs_trans *t, *tmp;

	pohmelfs_route_remove_all(st);

	mutex_lock(&st->trans_lock);
	list_for_each_entry_safe(t, tmp, &st->trans_list, trans_entry) {
		list_del(&t->trans_entry);

		pohmelfs_trans_put(t);
	}

	while (1) {
		struct rb_node *n = rb_first(&st->trans_root);
		if (!n)
			break;

		t = rb_entry(n, struct pohmelfs_trans, trans_node);

		rb_erase(&t->trans_node, &st->trans_root);
		pohmelfs_trans_put(t);
	}
	mutex_unlock(&st->trans_lock);

	cancel_work_sync(&st->io_work);
}

void pohmelfs_state_kill(struct pohmelfs_state *st)
{
	BUG_ON(!list_empty(&st->state_entry));

	pohmelfs_state_clean(st);
	pohmelfs_state_put(st);
}

void pohmelfs_state_schedule(struct pohmelfs_state *st)
{
	if (!st->conn->need_exit)
		queue_work(st->conn->wq, &st->io_work);
}

int pohmelfs_state_add_reconnect(struct pohmelfs_state *st)
{
	struct pohmelfs_connection *conn = st->conn;
	struct pohmelfs_reconnect *r, *tmp;
	int err = 0;

	pohmelfs_route_remove_all(st);

	r = kzalloc(sizeof(struct pohmelfs_reconnect), GFP_NOIO);
	if (!r) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	memcpy(&r->sa, &st->sa, sizeof(struct sockaddr_storage));
	r->addrlen = st->addrlen;
	r->group_id = st->group_id;

	mutex_lock(&conn->reconnect_lock);
	list_for_each_entry(tmp, &conn->reconnect_list, reconnect_entry) {
		if (tmp->addrlen != r->addrlen)
			continue;

		if (memcmp(&tmp->sa, &r->sa, r->addrlen))
			continue;

		err = -EEXIST;
		break;
	}

	if (!err) {
		list_add_tail(&r->reconnect_entry, &conn->reconnect_list);
	}
	mutex_unlock(&conn->reconnect_lock);

	if (err)
		goto err_out_free;

	pohmelfs_print_addr(&st->sa, "reconnection added\n");
	err = 0;
	goto err_out_exit;

err_out_free:
	kfree(r);
err_out_exit:

	spin_lock(&conn->state_lock);
	list_move(&st->state_entry, &conn->kill_state_list);
	spin_unlock(&conn->state_lock);

	/* we do not really care if this work will not be processed immediately */
	queue_delayed_work(conn->wq, &conn->reconnect_work, 0);

	return err;
}
