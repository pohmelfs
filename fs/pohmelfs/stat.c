/*
 * Copyright (C) 2011+ Evgeniy Polyakov <zbr@ioremap.net>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include "pohmelfs.h"

static int pohmelfs_stat_init(struct pohmelfs_trans *t)
{
	struct pohmelfs_wait *wait = t->priv;

	atomic_long_inc(&wait->count);
	pohmelfs_wait_get(wait);
	return 0;
}

static void pohmelfs_stat_destroy(struct pohmelfs_trans *t)
{
	struct pohmelfs_wait *wait = t->priv;

	atomic_long_dec(&wait->count);
	wake_up(&wait->wq);
	pohmelfs_wait_put(wait);
}

static int pohmelfs_stat_complete(struct pohmelfs_trans *t, struct pohmelfs_state *recv)
{
	struct pohmelfs_wait *wait = t->priv;
	struct dnet_cmd *cmd = &recv->cmd;
	struct dnet_attr *attr;
	int err = cmd->status;

	if (err)
		goto err_out_exit;

	if (cmd->size != sizeof(struct dnet_attr) + sizeof(struct dnet_stat)) {
		err = -ENOENT;
		goto err_out_exit;
	}

	attr = t->recv_data;

	if ((cmd->flags & DNET_FLAGS_MORE) && (attr->cmd == DNET_CMD_STAT) && (attr->size == sizeof(struct dnet_stat))) {
		struct dnet_stat *stat;

		stat = t->recv_data + sizeof(struct dnet_attr);
		dnet_convert_stat(stat);

		recv->bsize = stat->bsize;
		recv->frsize = stat->frsize;
		recv->blocks = stat->blocks;
		recv->bfree = stat->bfree;
		recv->bavail = stat->bavail;

		pr_debug("%s: total: %llu, avail: %llu\n",
			 pohmelfs_dump_id(cmd->id.id),
			 (unsigned long long)(stat->frsize * stat->blocks / 1024 / 1024),
			 (unsigned long long)(stat->bavail * stat->bsize / 1024 / 1024));
	}

err_out_exit:
	if (err)
		wait->condition = err;
	else
		wait->condition = 1;
	wake_up(&wait->wq);

	return 0;
}

int pohmelfs_stat(struct pohmelfs_sb *psb, int sync)
{
	struct pohmelfs_state **states, *st;
	struct pohmelfs_wait *wait;
	struct pohmelfs_io *pio;
	int err, i, num;
	long ret;

	wait = pohmelfs_wait_alloc(psb->root);
	if (!wait) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	pio = kmem_cache_zalloc(pohmelfs_io_cache, GFP_NOIO);
	if (!pio) {
		err = -ENOMEM;
		goto err_out_put;
	}

	err = pohmelfs_grab_states(psb, &states);
	if (err < 0)
		goto err_out_free_pio;

	pio->pi = psb->root;
	/* we use state pointer, but do not know correct ID, so use DIRECT flag here to forbid request forwarding */
	pio->cflags = DNET_FLAGS_NEED_ACK | DNET_FLAGS_NOLOCK | DNET_FLAGS_DIRECT;
	pio->cmd = DNET_CMD_STAT;
	pio->priv = wait;
	pio->cb.init = pohmelfs_stat_init;
	pio->cb.destroy = pohmelfs_stat_destroy;
	pio->cb.complete = pohmelfs_stat_complete;

	num = err;
	for (i = 0; i < num; ++i) {
		st = states[i];

		pio->group_id = st->group_id;
		pio->id = &psb->root->id;

		err = pohmelfs_send_buf_single(pio, st);
		pohmelfs_state_put(st);
	}

	err = 0;

	if (sync) {
		ret = wait_event_interruptible_timeout(wait->wq,
				atomic_long_read(&wait->count) != 0,
				msecs_to_jiffies(psb->read_wait_timeout));
		if (ret <= 0) {
			err = ret;
			if (ret == 0)
				err = -ETIMEDOUT;
			goto err_out_free;
		}

		if (wait->condition < 0)
			err = wait->condition;
	}

err_out_free:
	kfree(states);
err_out_free_pio:
	kmem_cache_free(pohmelfs_io_cache, pio);
err_out_put:
	pohmelfs_wait_put(wait);
err_out_exit:
	return err;
}
