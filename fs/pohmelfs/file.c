/*
 * Copyright (C) 2011+ Evgeniy Polyakov <zbr@ioremap.net>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/fs.h>

#include "pohmelfs.h"

#define POHMELFS_READ_LATEST_GROUPS_SCRIPT		"pohmelfs_read_latest_groups.py"

static int pohmelfs_write_init(struct pohmelfs_trans *t)
{
	struct pohmelfs_wait *wait = t->priv;

	pohmelfs_wait_get(wait);
	return 0;
}

static void pohmelfs_write_destroy(struct pohmelfs_trans *t)
{
	struct pohmelfs_wait *wait = t->priv;

	wake_up(&wait->wq);
	pohmelfs_wait_put(wait);
}

static int pohmelfs_write_complete(struct pohmelfs_trans *t, struct pohmelfs_state *recv)
{
	struct pohmelfs_wait *wait = t->priv;
	struct pohmelfs_inode *pi = pohmelfs_inode(t->inode);
	struct dnet_cmd *cmd = &recv->cmd;
	unsigned long long trans = cmd->trans & ~DNET_TRANS_REPLY;

	pr_debug("%s: %llu, flags: %x, status: %d\n",
		 pohmelfs_dump_id(pi->id.id), trans, cmd->flags, cmd->status);

	if (cmd->flags & DNET_FLAGS_MORE)
		return 0;

	wait->condition = cmd->status;
	if (!wait->condition)
		wait->condition = 1;

	return 0;
}

static int pohmelfs_send_write_metadata(struct pohmelfs_inode *pi, struct pohmelfs_io *pio, struct pohmelfs_wait *wait)
{
	struct pohmelfs_sb *psb = pohmelfs_sb(pi->vfs_inode.i_sb);
	struct timespec ts = CURRENT_TIME;
	struct dnet_meta_update *mu;
	struct dnet_meta *m;
	int err, size;
	void *data;

	size = sizeof(struct dnet_meta) * 4 +
			sizeof(struct dnet_meta_check_status) +
			sizeof(struct dnet_meta_update) +
			psb->fsid_len +
			psb->group_num * sizeof(int);

	data = kzalloc(size, GFP_NOIO);
	if (!data) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	m = data;
	m->type = DNET_META_GROUPS;
	m->size = psb->group_num * sizeof(int);
	memcpy(m->data, psb->groups, m->size);
	dnet_convert_meta(m);

	m = (struct dnet_meta *)(m->data + le32_to_cpu(m->size));
	m->type = DNET_META_NAMESPACE;
	m->size = psb->fsid_len;
	memcpy(m->data, psb->fsid, psb->fsid_len);
	dnet_convert_meta(m);

	m = (struct dnet_meta *)(m->data + le32_to_cpu(m->size));
	m->type = DNET_META_UPDATE;
	m->size = sizeof(struct dnet_meta_update);
	mu = (struct dnet_meta_update *)m->data;
	mu->tm.tsec = ts.tv_sec;
	mu->tm.tnsec = ts.tv_nsec;
	dnet_convert_meta_update(mu);
	dnet_convert_meta(m);

	m = (struct dnet_meta *)(m->data + le32_to_cpu(m->size));
	m->type = DNET_META_CHECK_STATUS;
	m->size = sizeof(struct dnet_meta_check_status);
	/* do not fill, it will be updated on server */
	dnet_convert_meta(m);

	pio->pi = pi;
	pio->id = &pi->id;
	pio->cmd = DNET_CMD_WRITE;
	pio->ioflags = DNET_IO_FLAGS_OVERWRITE | DNET_IO_FLAGS_META;
	pio->cflags = DNET_FLAGS_NEED_ACK;
	pio->type = 1;
	pio->cb.init = pohmelfs_write_init;
	pio->cb.destroy = pohmelfs_write_destroy;
	pio->cb.complete = pohmelfs_write_complete;
	pio->priv = wait;
	pio->data = data;
	pio->size = size;

	err = pohmelfs_send_io(pio);
	if (err)
		goto err_out_free;

err_out_free:
	kfree(data);
err_out_exit:
	return err;
}

static int pohmelfs_write_command_complete(struct pohmelfs_trans *t, struct pohmelfs_state *recv)
{
	struct dnet_cmd *cmd = &recv->cmd;
	struct pohmelfs_write_ctl *ctl = t->wctl;

	if (cmd->flags & DNET_FLAGS_MORE)
		return 0;

	if (cmd->status == 0)
		atomic_inc(&ctl->good_writes);
	else {
		struct inode *inode = t->inode;
		struct pohmelfs_inode *pi = pohmelfs_inode(inode);
		unsigned long long size = le64_to_cpu(t->cmd.p.io.size);
		unsigned long long offset = le64_to_cpu(t->cmd.p.io.offset);

		pr_debug("%s: write failed: ino: %lu, isize: %llu, offset: %llu, size: %llu: %d\n",
			 pohmelfs_dump_id(pi->id.id), inode->i_ino,
			 inode->i_size, offset, size, cmd->status);
	}

	return 0;
}

static int pohmelfs_write_command_init(struct pohmelfs_trans *t)
{
	struct pohmelfs_write_ctl *ctl = t->wctl;

	kref_get(&ctl->refcnt);
	return 0;
}

static void pohmelfs_write_command_destroy(struct pohmelfs_trans *t)
{
	struct pohmelfs_write_ctl *ctl = t->wctl;

	kref_put(&ctl->refcnt, pohmelfs_write_ctl_release);
}

int pohmelfs_write_command(struct pohmelfs_inode *pi, struct pohmelfs_write_ctl *ctl, loff_t offset, size_t len)
{
	int err;
	struct inode *inode = &pi->vfs_inode;
	struct pohmelfs_io *pio;
	uint64_t prepare_size = i_size_read(&pi->vfs_inode);

	pio = kmem_cache_zalloc(pohmelfs_io_cache, GFP_NOIO);
	if (!pio) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	pio->pi = pi;
	pio->id = &pi->id;
	pio->cmd = DNET_CMD_WRITE;
	pio->offset = offset;
	pio->size = len;
	pio->cflags = DNET_FLAGS_NEED_ACK;

	/*
	 * We always set prepare bit, since elliptics/eblob reuses existing (previously prepared/reserved) area
	 * But it also allows to 'miss' prepare message (for example if we sent prepare bit when node was offline)
	 */
	pio->ioflags = DNET_IO_FLAGS_OVERWRITE | DNET_IO_FLAGS_PLAIN_WRITE | DNET_IO_FLAGS_PREPARE;

	pio->num = prepare_size;

	/* commit when whole inode is written */
	if (offset + len == prepare_size) {
		pio->ioflags |= DNET_IO_FLAGS_COMMIT;
	}

	pio->wctl = ctl;
	pio->priv = ctl;
	pio->cb.complete = pohmelfs_write_command_complete;
	pio->cb.init = pohmelfs_write_command_init;
	pio->cb.destroy = pohmelfs_write_command_destroy;

	pr_debug("%s: ino: %lu, offset: %llu, len: %zu, total size: %llu\n",
		 pohmelfs_dump_id(pi->id.id), inode->i_ino,
		 (unsigned long long)offset, len, inode->i_size);

	err = pohmelfs_send_io(pio);
	if (err)
		goto err_out_free;

err_out_free:
	kmem_cache_free(pohmelfs_io_cache, pio);
err_out_exit:
	return err;
}

int pohmelfs_metadata_inode(struct pohmelfs_inode *pi, int sync)
{
	struct inode *inode = &pi->vfs_inode;
	struct pohmelfs_sb *psb = pohmelfs_sb(inode->i_sb);
	struct pohmelfs_io *pio;
	struct pohmelfs_wait *wait;
	long ret;
	int err;

	wait = pohmelfs_wait_alloc(pi);
	if (!wait) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	pio = kmem_cache_zalloc(pohmelfs_io_cache, GFP_NOIO);
	if (!pio) {
		err = -ENOMEM;
		goto err_out_put;
	}

	err = pohmelfs_send_write_metadata(pi, pio, wait);
	if (err)
		goto err_out_free;

	if (sync) {
		ret = wait_event_interruptible_timeout(wait->wq,
				wait->condition != 0 && atomic_read(&wait->refcnt.refcount) <= 2,
				msecs_to_jiffies(psb->write_wait_timeout));
		if (ret <= 0) {
			err = ret;
			if (ret == 0)
				err = -ETIMEDOUT;
			goto err_out_free;
		}

		if (wait->condition < 0) {
			err = wait->condition;
			goto err_out_free;
		}
	}

err_out_free:
	kmem_cache_free(pohmelfs_io_cache, pio);
err_out_put:
	pohmelfs_wait_put(wait);
err_out_exit:
	return err;
}

static long pohmelfs_fallocate(struct file *file, int mode, loff_t offset, loff_t len)
{
	struct inode *inode = file->f_path.dentry->d_inode;
	struct pohmelfs_inode *pi = pohmelfs_inode(inode);
	struct pohmelfs_io *pio;
	int err;

	if (offset + len < i_size_read(inode)) {
		err = 0;
		goto err_out_exit;
	}

	pio = kmem_cache_zalloc(pohmelfs_io_cache, GFP_NOIO);
	if (!pio) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	pio->pi = pi;
	pio->id = &pi->id;
	pio->cmd = DNET_CMD_WRITE;
	pio->cflags = DNET_FLAGS_NEED_ACK;
	pio->ioflags = DNET_IO_FLAGS_PREPARE;
	pio->num = i_size_read(inode);

	pr_debug("%s: ino: %lu, offset: %llu, len: %llu, total size: %llu\n",
		 pohmelfs_dump_id(pi->id.id), inode->i_ino,
		 (unsigned long long)offset, (unsigned long long)len,
		 inode->i_size);

	err = pohmelfs_send_io(pio);
	if (err)
		goto err_out_free;

err_out_free:
	kmem_cache_free(pohmelfs_io_cache, pio);
err_out_exit:
	return err;
}

struct pohmelfs_latest_ctl {
	struct dnet_id			id;
	uint64_t			offset;
	uint64_t			size;
};

static int pohmelfs_read_latest_complete(struct pohmelfs_trans *t, struct pohmelfs_state *recv)
{
	struct pohmelfs_inode *pi = pohmelfs_inode(t->inode);
	struct pohmelfs_wait *wait = t->priv;
	struct dnet_cmd *cmd = &recv->cmd;
	int err = cmd->status;

	if (cmd->status)
		goto err_out_exit;

	if (cmd->flags & DNET_FLAGS_MORE) {
		pr_debug("%s: group: %d, attr size: %lld\n",
			 pohmelfs_dump_id(cmd->id.id), cmd->id.group_id,
			 cmd->size - sizeof(struct dnet_attr));
		if (cmd->size < sizeof(struct dnet_attr) + 4) {
			err = -ENOENT;
			goto err_out_exit;
		}

		mutex_lock(&pi->lock);
		if (!pi->groups) {
			pi->groups = kmalloc(cmd->size - sizeof(struct dnet_attr), GFP_NOIO);
			if (!pi->groups) {
				err = -ENOMEM;
				mutex_unlock(&pi->lock);
				goto err_out_exit;
			}

			pi->group_num = (cmd->size - sizeof(struct dnet_attr)) / sizeof(int);
			memcpy(pi->groups, t->recv_data + sizeof(struct dnet_attr), pi->group_num * sizeof(int));

			pr_debug("%s: group: %d, received: %d groups\n",
				 pohmelfs_dump_id(cmd->id.id), cmd->id.group_id,
				 pi->group_num);
		}
		mutex_unlock(&pi->lock);
	}

err_out_exit:
	if (err)
		wait->condition = err;
	else
		wait->condition = 1;
	return 0;
}

static int pohmelfs_read_latest_group(struct pohmelfs_inode *pi, struct pohmelfs_latest_ctl *r, int group_id)
{
	struct pohmelfs_script_req req;

	memset(&req, 0, sizeof(struct pohmelfs_script_req));

	req.script_name = POHMELFS_READ_LATEST_GROUPS_SCRIPT;
	req.script_namelen = sizeof(POHMELFS_READ_LATEST_GROUPS_SCRIPT) - 1;

	req.obj_name = "noname";
	req.obj_len = 5;

	req.binary = r;
	req.binary_size = sizeof(struct pohmelfs_latest_ctl);

	req.id = &pi->id;
	req.group_id = group_id;
	req.sync = 1;
	req.cflags = 0;
	req.complete = pohmelfs_read_latest_complete;

	return pohmelfs_send_script_request(pi, &req);
}

static int pohmelfs_read_latest(struct pohmelfs_inode *pi)
{
	struct pohmelfs_latest_ctl *r;
	struct pohmelfs_sb *psb = pohmelfs_sb(pi->vfs_inode.i_sb);
	int i, err = -ENOENT;

	r = kzalloc(sizeof(struct pohmelfs_latest_ctl), GFP_NOIO);
	if (!r) {
		err = -ENOMEM;
		goto err_out_exit;
	}

	dnet_setup_id(&r->id, 0, pi->id.id);

	for (i = 0; i < psb->group_num; ++i) {
		r->id.group_id = psb->groups[i];

		err = pohmelfs_read_latest_group(pi, r, psb->groups[i]);
		if (err)
			continue;

		break;
	}

	kfree(r);

	pr_debug("%s: %d groups\n", pohmelfs_dump_id(pi->id.id), pi->group_num);

err_out_exit:
	return err;
}

static int pohmelfs_file_open(struct inode *inode, struct file *filp)
{
	struct pohmelfs_inode *pi = pohmelfs_inode(inode);

	if (!pi->group_num && !pi->local)
		pohmelfs_read_latest(pi);

	if (pohmelfs_need_resync(pi))
		invalidate_mapping_pages(&inode->i_data, 0, -1);

	return generic_file_open(inode, filp);
}

/*
 * We want fsync() to work on POHMELFS.
 */
static int pohmelfs_fsync(struct file *filp, loff_t start, loff_t end, int datasync)
{
	struct inode *inode = filp->f_mapping->host;
	int err = filemap_write_and_wait_range(inode->i_mapping, start, end);
	if (!err) {
		mutex_lock(&inode->i_mutex);
		err = sync_inode_metadata(inode, 1);
		mutex_unlock(&inode->i_mutex);
	}
	pr_debug("%s: start: %lld, end: %lld, nrpages: %ld, dirty: %d: %d\n",
		 pohmelfs_dump_id(pohmelfs_inode(inode)->id.id),
		 (unsigned long long)start, (unsigned long long)end,
		 inode->i_mapping->nrpages,
		 mapping_cap_writeback_dirty(inode->i_mapping), err);
	return err;
}

static int pohmelfs_flush(struct file *filp, fl_owner_t id)
{
	struct inode *inode = filp->f_mapping->host;
	struct pohmelfs_sb *psb = pohmelfs_sb(inode->i_sb);
	int err = 0;

	if (psb->sync_on_close)
		err = pohmelfs_fsync(filp, 0, ~0ULL, 1);

	if (!err && test_bit(AS_EIO, &inode->i_mapping->flags))
		err = -EIO;

	pr_debug("%s: %d\n",
		 pohmelfs_dump_id(pohmelfs_inode(inode)->id.id), err);
	return err;
}

const struct file_operations pohmelfs_file_ops = {
	.open		= pohmelfs_file_open,

	.llseek		= generic_file_llseek,

	.read		= do_sync_read,
	.aio_read	= generic_file_aio_read,

	.mmap		= generic_file_mmap,

	.splice_read	= generic_file_splice_read,
	.splice_write	= generic_file_splice_write,

	.write		= do_sync_write,
	.aio_write	= generic_file_aio_write,

	.fallocate	= pohmelfs_fallocate,

	.fsync		= pohmelfs_fsync,
	.flush		= pohmelfs_flush,
};

const struct inode_operations pohmelfs_file_inode_operations = {
};
